from django.urls import path
from . import views

app_name = 'appLab7'

urlpatterns = [
    path('index/', views.index, name='index'),
    path('', views.redirecting, name='redirecting'),
]